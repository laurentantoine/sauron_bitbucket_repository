﻿using Mapbox.Unity.Map;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CreateurDElements : NetworkBehaviour {

    [SerializeField]
    AbstractMap _map;


    [SerializeField]
    GameObject _ElementPrefab;


    [SerializeField]
    float _spawnScale = 10f;
    
    float facteurScale = 0.05f;

    List<GameObject> _spawnedObjects;

    Transform repereSpawn;


    private GameObject gameObjectTampon;


    // Use this for initialization
    void Start () {
        _spawnedObjects = new List<GameObject>();
        gameObjectTampon = new GameObject();
    }
	
	// Update is called once per frame
	void Update () {

        
        if (Input.GetMouseButtonDown(0))
        {
            CmdNouvelElement();
        }



        int count = _spawnedObjects.Count;
        for (int i = 0; i < count; i++)
        {
            var spawnedObject = _spawnedObjects[i];
            spawnedObject.transform.localPosition = _map.GeoToWorldPosition(spawnedObject.GetComponent<ElementScript>().geoPosition);
            spawnedObject.transform.localScale = new Vector3(_spawnScale, _spawnScale, _spawnScale);
        }


    }

    [Command]
    void CmdNouvelElement()
    {
        float mouseX = Input.mousePosition.x;
        float mouseY = Input.mousePosition.y;
        Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(mouseX, mouseY, Camera.main.nearClipPlane));

        gameObjectTampon.transform.position = pos;

        facteurScale = ScaleCorrector(_map.WorldRelativeScale);

        double geoPosX = (pos.x * _map.WorldRelativeScale * facteurScale) + _map.CenterMercator.x;
        double geoPosY = (pos.z * _map.WorldRelativeScale * facteurScale) + _map.CenterMercator.y;

        Vector2d geoPos = Conversions.MetersToLatLon(new Vector2d(geoPosX, geoPosY));


        _spawnedObjects.Add(newElement(geoPos));
    }

    private GameObject newElement(Vector2d geoPos)
    {
        var instance = Instantiate(_ElementPrefab);
        instance.transform.localPosition = _map.GeoToWorldPosition(geoPos);
        instance.transform.localScale = Vector3.one * _spawnScale;
        instance.GetComponent<ElementScript>().geoPosition = geoPos;
        NetworkServer.Spawn(instance);
        return instance;
    }

    private float ScaleCorrector(float WorldRelativeScale)
    {
        return 666.66f * Mathf.Pow(WorldRelativeScale, -2.002f);
    }
}


