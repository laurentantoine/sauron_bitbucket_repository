﻿using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;


public class ElementScript : NetworkBehaviour {

    [SyncVar]
    public string description;

    [SyncVar]
    public int effectif;

    [SyncVar]
    public int indiceImage;

    [SyncVar]
    public string nom;

    [SyncVar]
    public Vector2d geoPosition;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
